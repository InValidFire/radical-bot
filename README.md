# Radical Bot 2

Newest version of the Radical Bot! <3

## Requirements
This bot will be programmed with the following requirements:
- Everything must be async compatible, no blocking code other than filesystem calls.
- Handle authentication in an intelligent way using OAuth2.
- Easy to update and migrate to new versions of the bot.

## Extensions
The following extensions are planned for this bot:
- System - general system methods, general maintenance and informative commands.
- Rooms - assist with LibCal room booking.

### System

#### Commands
- update (bot admin only) - update the bot, if deployed update to latest release tag, if development update to latest commit.
- restart (bot admin only) - restart the bot.
- info - lists bot information, such as version and GitHub address.

### Rooms
- book - book rooms, requires an image of a LibCal reservation e-mail.
- rooms - list upcoming rooms

## License

This project is licensed under CNPL-NA+