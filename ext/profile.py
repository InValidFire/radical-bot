import hikari
import lightbulb

plugin = lightbulb.Plugin("ProfilePlugin")


@plugin.command
@lightbulb.option("img", "image to set as new avatar", type=hikari.Attachment, required=True)
@lightbulb.add_checks(lightbulb.owner_only)
@lightbulb.command("avatar", "set the bot avatar.", ephemeral=True)
@lightbulb.implements(lightbulb.SlashCommand)
async def set_avatar(ctx: lightbulb.Context) -> None:
    await ctx.bot.rest.edit_my_user(avatar=ctx.options.img)
    embed = hikari.Embed(title="New avatar set!")
    embed.set_image(ctx.options.img)
    await ctx.respond(embed)


@plugin.command
@lightbulb.option("text", "text to set as custom status", type=str, required=True)
@lightbulb.option("type", "type of status, 0 is Playing, 1 is Watching, 2 is Streaming, 3 is Listening, 4 is Competing", type=int, default=0)
@lightbulb.add_checks(lightbulb.owner_only)
@lightbulb.command("status", "set the bot status.", ephemeral=True)
@lightbulb.implements(lightbulb.SlashCommand)
async def set_status(ctx: lightbulb.Context) -> None:
    choices = [hikari.ActivityType.PLAYING, hikari.ActivityType.WATCHING,
               hikari.ActivityType.STREAMING, hikari.ActivityType.LISTENING, hikari.ActivityType.COMPETING]
    await ctx.bot.update_presence(activity=hikari.Activity(name=ctx.options.text, type=choices[ctx.options.type]))
    embed = hikari.Embed(title="New status set!",
                         description=f"New Status: {ctx.options.text}")
    await ctx.respond(embed)


def load(bot: lightbulb.BotApp):
    bot.add_plugin(plugin)


def unload(bot: lightbulb.BotApp):
    bot.remove_plugin(plugin)
