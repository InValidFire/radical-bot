import os
from pathlib import Path

user = input("Enter name of server's system user: ")
root = Path(input("Enter the path of the server's root directory: "))

if not root.exists():
    $(mkdir @(root))
    print(f"Made server directory at {root}")

$(useradd --system @(user))
with Path("service.service").open("r") as fp:
    text = fp.read()
    text = text.replace("$user$", user)
    text = text.replace("$root$", str(root))
$(cp service.service @(user).service)
with Path(f"{user}.service").open("w+") as fp:
    fp.write(text)
$(cp @(user).service /etc/systemd/system)
$(cd @(root))
$(systemctl enable @(user).service)