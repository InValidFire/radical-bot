import lightbulb
import miru

from lib.config import load_config

config = load_config()

bot = lightbulb.BotApp(config.discord_token, default_enabled_guilds=[config.discord_guild_id])
bot.load_extensions_from("ext/")
miru.load(bot)

bot.run()
