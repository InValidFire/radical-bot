from datetime import datetime
import dateutil.parser as parser


class Room:
    def __init__(self, number: str, start_time: datetime, end_time: datetime):
        self.number: str = number
        self.start_time: datetime = start_time
        self.end_time: datetime = end_time

    @classmethod
    def from_event_dict(cls, body: dict):
        start_time = parser.parse(body['start']['dateTime'])
        end_time = parser.parse(body['end']['dateTime'])
        return cls(body['summary'], start_time, end_time)

    def get_time_str(self) -> str:
        date = self.start_time.strftime("%B %d")
        start = self.start_time.strftime("%H:%M")
        end = self.end_time.strftime("%H:%M")
        return f"{date}, {start} - {end}"
