import json
from pathlib import Path
from dataclasses import dataclass

@dataclass
class Config(object):
    discord_token: str | None = None
    discord_guild_id: int | None = None
    discord_study_room_thread: int | None = None
    git_branch: str | None = None
    git_url: str | None = None
    google_calendar_id: str | None = None
    time_zone: str | None = None

def config_decoder(obj):
    if '__type__' in obj and obj['__type__'] == "Config":
        return Config(
            discord_token=obj['discord_token'],
            discord_guild_id=obj['discord_guild_id'],
            discord_study_room_thread=obj['discord_study_room_thread'],
            git_branch=obj['git_branch'],
            git_url=obj['git_url'], 
            google_calendar_id=obj['google_calendar_id'],
            time_zone=obj['time_zone']
        )

def save_config(config: dict):
    with Path("config.json").open("w+", encoding="utf-8") as fp:
        json.dump(config, fp, indent=4)

def load_config():
    with Path("config.json").open("r", encoding="utf-8") as fp:
        config = json.load(fp, object_hook=config_decoder)
    return config
